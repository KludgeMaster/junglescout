export class Product {
    constructor (
        public asin: string, 
        public category: string, 
        public rating: string, 
        public dimensions: string) {}
}