import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) { }

  getProduct(asin: string) {
    return this.http.get(baseApiUrl, { 
      params: { asin: asin }
    });
  }
}


// TODO: to config file
const baseApiUrl = "http://localhost:3000/api/product";