import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/service/http.service';
import { Product } from '../../model/product.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchStr = "";
  product = null;
  error = "";

  constructor(private http: HttpService) { }

  ngOnInit() {
  }
  
  search() {
    this.product = null;
    this.error = null;
    // TODO: show loading...
    this.http.getProduct(this.searchStr).subscribe((data: any) => {
      this.product = new Product(data.asin, data.category, data.rating, data.dimensions)
    }, (err) => {
      if (err) {
        this.error = err.error || "Error Occurred..."
      }
    });
  }
}
