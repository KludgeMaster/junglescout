const express = require('express');
// const database = require('./app/config/dbconfig');
const cors = require('cors');

const app = express();

// allow all origin in dev
app.use(cors());
const port = 3000; // default port
app.listen(port, () => {
    console.log("Server is listening on port " + port);
});

const API_ROOT = '/api';
app.use(API_ROOT, require('./app/api/api'));