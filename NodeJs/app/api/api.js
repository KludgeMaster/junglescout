const express = require('express');
const router = express.Router();

const ProductController = require('../controller/productController');
const productController = new ProductController();

router.get('/product', (req, res) => {
    productController.findByAsin(req, res);
});

module.exports = router;
