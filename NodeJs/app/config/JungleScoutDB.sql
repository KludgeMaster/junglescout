CREATE TABLE if not exists Products (
	asin TEXT PRIMARY KEY,
	category TEXT,
	rating TEXT,
	dimensions TEXT
)