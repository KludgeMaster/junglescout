const sqlite3 = require('sqlite3').verbose();
const path = require('path');

// Load DB 
const dbPath = path.resolve(__dirname, "JungleScoutDB.db");
console.log(__dirname);
// const db = new sqlite3.Database('../database/JungleScoutDB.db');
const db = new sqlite3.Database(dbPath);


module.exports = {
    db: db
};

