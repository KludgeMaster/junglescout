class Product {
    constructor(asin, category, rating, dimension) {
        this.asin = asin;
        this.category = category;
        this.rating = rating;
        this.dimension = dimension;
    }
}

module.export = Product;