const ProductDao = require('../dao/productDao');
const AmazonScraper = require('./scraper');
const amazonScraper = new AmazonScraper();

class ProductController {
	constructor() {
		this.productDao = new ProductDao();
	}

	findByAsin(req, res) {
		let asin = req.query && req.query.asin;
		if (asin) {
			amazonScraper.scrapeWithAsin(asin).then((result) => {
				// TODO: check if asin already exists in db
				// if exists, update existing record insteads
				this.productDao.create(result).then(() => {
					res.status(200); // 200 OK
					res.json(result);
				});
			}).catch((err) => {
				res.status(500); // TODO: handle errors depending on failed points
				res.json(err.message);
			})
		} else {
			res.status(400);
			res.json("ASIN Missing");
		}
	}
}

module.exports = ProductController;