
const axios = require('axios');
const $ = require('cheerio');

// to be moved to config or const file
const amazonUrl = "http://www.amazon.com/dp/";
const ParserSelector = {
    category : "#wayfinding-breadcrumbs_feature_div ul > li:first-child",
    rating: "#averageCustomerReviews_feature_div i.a-icon-star > span",
    dimensions: "#prodDetails td.label:contains('Product Dimensions') + td.value"
};

class AmazonScraper {
    constructor() {
        this.parserSelector = ParserSelector;
    }

    scrapeWithAsin (asin) {
        return this.getProductInfo(asin);
    }

    getProductInfo(asin) {
        let result = {};
        result.asin = asin;

        return axios.get(amazonUrl + asin).then(res => {
            if (res && res.data) {
                return new Promise((resolve, reject) => {
                    resolve(this.parseHtml(res.data, result));
                });
            } else {
                throw new Error ("No Response");
            }
            
        }).catch(err => {
            console.log("status: ", err.response.status)
            if (err.response.status === 404) { // TODO: use enum
                throw new Error("Item Not Found");
            }
            throw new Error("Internal Server Error");
        });
    }

    parseHtml(html, result) {
        for (let field in this.parserSelector) {
            result[field] = $(this.parserSelector[field], html).text().trim();
        }
        return result; 
    }
}

module.exports = AmazonScraper;