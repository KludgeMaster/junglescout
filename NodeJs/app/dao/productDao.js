const Product = require('../model/product');
const database = require('../config/dbconfig');

class ProductDao {
    constructor() {}
    
    findByAsin(asin) {
        let sqlRequest = "SELECT ASIN FROM Products where asin = $asin";
        let sqlParam = {$asin: asin};
        return new Promise ((resolve, reject) => {
	        database.db.all(sqlRequest, sqlParam, (err, rows) => {
	        	if (err) 
	        		reject ( new Error("Server Error"));
	        	else if (rows === null || rows.length === 0) 
	        		reject (new Error("No Entity Found"));
	        	else {
	        		let row = rows[0];
					let product = new Product(row.asin, row.category, row.rating, row.dimension);
	        		resolve(product);
	        	}
	        });
        }); 
	}
	
	create(product) {
		let sqlRequest = "INSERT into Products (asin, category, rating, dimensions) " +
			"VALUES ($asin, $category, $rating, $dimensions)";
		let sqlParam = {
			$asin: product.asin,
			$category: product.category,
			$rating: product.rating,
			$dimensions: product.dimensions
		};
		return new Promise ((resolve, reject) => {
			let db = database.db.prepare(sqlRequest);
			db.run(sqlParam, (err) => {
				console.log('this',this);
				console.log('err', err);
				resolve();
			});
		});
	}
}

module.exports = ProductDao;
