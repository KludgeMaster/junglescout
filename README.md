# Amazon ASIN Scraper 

This project is to scrape product details of Amazon products based on their ASIN.
This project is implemented with Node JS RESTful API, Angular front-end, and SQLite database.

## Installation
 - Clone this repository: `https://gitlab.com/KludgeMaster/junglescout.git`
 - Go to /NodeJS folder and install its dependencies: `cd NodeJs && npm install`
 - From root folder, go to /Angular folder and install its dependencies: `cd Angular && npm install`

## Getting Started
1. Start the Node server by `node index.js` in NodeJS folder
2. Start Angular by `ng serve` in Angular folder
3. In browser, go to `http://localhost:4200`
4. Enter ASIN in search box

## Creator
- Ryan Song (https://gitlab.com/KludgeMaster)

## License
[MIT License ](https://gitlab.com/KludgeMaster/junglescout/blob/master/LICENSE.md)